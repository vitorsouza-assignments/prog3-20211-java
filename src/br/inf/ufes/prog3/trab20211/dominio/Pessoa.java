package br.inf.ufes.prog3.trab20211.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class Pessoa implements Comparable<Pessoa>, Serializable {
	private String nome;

	private Date dataNascimento;

	protected Pessoa(String nome) {
		this.nome = nome;
	}

	protected Pessoa(String nome, Date dataNascimento) {
		this(nome);
		this.dataNascimento = dataNascimento;
	}

	public int calcularIdade() {
		Calendar nascimento = Calendar.getInstance();
		nascimento.setTime(dataNascimento);
		Calendar agora = Calendar.getInstance();
		agora.setTime(new Date(System.currentTimeMillis()));
		return agora.get(Calendar.YEAR) - nascimento.get(Calendar.YEAR);
	}

	@Override
	public String toString() {
		return nome;
	}

	@Override
	public int compareTo(Pessoa o) {
		// Compara por ordem alfabética reversa de nome.
		Collator collator = Collator.getInstance(Locale.getDefault());
		collator.setStrength(Collator.PRIMARY);
		return collator.compare(o.nome, nome);
	}
}
