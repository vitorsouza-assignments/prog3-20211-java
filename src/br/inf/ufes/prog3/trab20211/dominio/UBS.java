package br.inf.ufes.prog3.trab20211.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import br.inf.ufes.prog3.trab20211.exceptions.UBSSemEstoqueException;

public class UBS implements Comparable<UBS>, Serializable {
	private String sigla;

	private String nome;

	private SortedSet<Lote> entregas = new TreeSet<>();

	private Set<Vacinacao> agendamentos = new HashSet<>();

	private Set<Cidadao> vacinados = new HashSet<>();

	private Date inicioVacinacao;

	private Date fimVacinacao;

	public UBS(String sigla, String nome) {
		this.sigla = sigla;
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	protected void agendarVacinacao(Vacinacao vacinacao) {
		agendamentos.add(vacinacao);

		// Verifica início e fim da vacinação.
		Date dataVacinacao = vacinacao.getDataHora();
		if (inicioVacinacao == null || inicioVacinacao.after(dataVacinacao)) {
			inicioVacinacao = dataVacinacao;
		}
		if (fimVacinacao == null || fimVacinacao.before(dataVacinacao)) {
			fimVacinacao = dataVacinacao;
		}
	}

	public void receberLote(Lote lote) {
		entregas.add(lote);
	}

	public void vacinar(Vacinacao vacinacao) throws UBSSemEstoqueException {
		Cidadao cidadao = vacinacao.getCidadao();
		Vacina vacina = vacinacao.getVacina();

		// Verifica se tem estoque da vacina e reduz uma dose caso tenha.
		Lote loteEscolhido = null;
		for (Lote lote : entregas) {
			if (lote.getVacina() == vacina && lote.getData().before(vacinacao.getDataHora())
					&& lote.getQuantidadeEmEstoque() > 0) {
				loteEscolhido = lote;
				loteEscolhido.retirarDose();
				break;
			}
		}
		if (loteEscolhido == null)
			throw new UBSSemEstoqueException(this, vacina, vacinacao.getDataHora());

		// Registra o cidadão vacinado.
		vacinados.add(cidadao);
	}

	public int getQtdCidadaosVacinados() {
		return vacinados.size();
	}

	public int getQtdCidadaosAVacinar() {
		Set<Cidadao> cidadaos = new HashSet<>();
		for (Vacinacao vacinacao : agendamentos) {
			if (!vacinacao.isCancelado() && !vacinacao.isEfetuado()) {
				cidadaos.add(vacinacao.getCidadao());
			}
		}
		return cidadaos.size();
	}

	public int getQtdAgendamentosCancelados() {
		int qtd = 0;
		for (Vacinacao vacinacao : agendamentos) {
			if (vacinacao.isCancelado()) {
				qtd++;
			}
		}
		return qtd;
	}

	public Date getInicioVacinacao() {
		return inicioVacinacao;
	}

	public Date getFimVacinacao() {
		return fimVacinacao;
	}

	@Override
	public String toString() {
		return nome;
	}

	@Override
	public int compareTo(UBS o) {
		// Compara primeiro por quantidade de agendamentos (decrescente).
		int cmp = o.agendamentos.size() - agendamentos.size();
		if (cmp != 0)
			return cmp;

		// Em seguida, ordem alfabética de nome.
		Collator collator = Collator.getInstance(Locale.getDefault());
		collator.setStrength(Collator.PRIMARY);
		return collator.compare(nome, o.nome);
	}
}