package br.inf.ufes.prog3.trab20211;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;

import br.inf.ufes.prog3.trab20211.dominio.Doenca;
import br.inf.ufes.prog3.trab20211.dominio.Lote;
import br.inf.ufes.prog3.trab20211.dominio.UBS;
import br.inf.ufes.prog3.trab20211.dominio.Vacina;
import br.inf.ufes.prog3.trab20211.exceptions.DomainException;
import br.inf.ufes.prog3.trab20211.io.Leitor;

/**
 * Aplicativo para verificar se os arquivos de testes produzidos pelos alunos passam o critério mínimo para ganhar 1
 * ponto extra.
 * 
 * @author Vitor E. Silva Souza (vitorsouza@gmail.com)
 */
public class AplConferirArquivosEntrada implements Serializable {
	/** Serialization ID. */
	private static final long serialVersionUID = 1L;

	/** Nome do arquivo que possui os dados de agendamentos. */
	private static final String NOME_ARQUIVO_AGENDAMENTOS = "agendamentos.csv";

	/** Nome do arquivo que possui os dados de lotes. */
	private static final String NOME_ARQUIVO_LOTES = "lotes.csv";

	/** Nome do arquivo que possui os dados de servidores. */
	private static final String NOME_ARQUIVO_SERVIDORES = "servidores.csv";

	/** Nome do arquivo que possui os dados de UBSs. */
	private static final String NOME_ARQUIVO_UNIDADES = "unidades.csv";

	/** Nome do arquivo que possui os dados de vacinas. */
	private static final String NOME_ARQUIVO_VACINAS = "vacinas.csv";

	/** Número mínimo de UBSs nos dados fornecidos. */
	private static final int MINIMO_UNIDADES = 4;

	/** Número mínimo de servidores nos dados fornecidos. */
	private static final int MINIMO_SERVIDORES = 6;

	/** Número mínimo de vacinas nos dados fornecidos. */
	private static final int MINIMO_VACINAS = 12;

	/** Número mínimo de doenças nos dados fornecidos. */
	private static final int MINIMO_DOENCAS = 3;

	/** Número mínimo de entregas por vacina nos dados fornecidos. */
	private static final int MINIMO_LOTES_POR_VACINA = 3;

	/** Número mínimo de agendamentos nos dados fornecidos. */
	private static final int MINIMO_AGENDAMENTOS = 20;

	/** Leitor de dados. */
	private Leitor leitor;

	/** Método principal. Cria uma instância da aplicação e executa. */
	public static void main(String[] args) throws ParseException, ClassNotFoundException {
		try {
			Locale.setDefault(new Locale("pt", "BR"));
			AplConferirArquivosEntrada apl = new AplConferirArquivosEntrada();
			apl.lerPlanilhas(NOME_ARQUIVO_AGENDAMENTOS, NOME_ARQUIVO_LOTES, NOME_ARQUIVO_SERVIDORES, NOME_ARQUIVO_UNIDADES, NOME_ARQUIVO_VACINAS);
			apl.executar();
		}
		catch (IOException e) {
			System.out.println("Erro de I/O.");
			e.printStackTrace();
		} catch (DomainException e) {
			System.out.printf("%s%n", e.getMessage());
			//e.printStackTrace();
		}
	}

	/** Lê as planilhas de entrada de dados especificadas. */
	private void lerPlanilhas(String nomeArquivoAgendamentos, String nomeArquivoLotes, String nomeArquivoServidores, String nomeArquivoUnidades, String nomeArquivoVacinas) throws IOException, DomainException {
		leitor = new Leitor(new File(nomeArquivoUnidades), new File(nomeArquivoServidores), new File(nomeArquivoVacinas), new File(nomeArquivoLotes), new File(nomeArquivoAgendamentos));
	}

	/**
	 * Executa a verificação dos arquivos.
	 * 
	 * @throws IOException
	 */
	private void executar() throws IOException {
		// Obtém os dados do leitor.
		SortedSet<UBS> unidades = leitor.listarUnidades();
		SortedSet<Doenca> doencas = leitor.listarDoencas();

		// Verifica os números absolutos de cada dado.
		int qtdUnidades = unidades.size();
		int qtdServidores = leitor.getQtdServidores();
		int qtdVacinas = leitor.getQtdVacinas();
		int qtdDoencas = doencas.size();
		int qtdAgendamentos = leitor.getQtdVacinacoes();

		Map<Vacina, Integer> lotesPorVacina = new HashMap<>();
		for (Lote lote : leitor.getLotes()) {
			Vacina vacina = lote.getVacina();
			Integer qtd = lotesPorVacina.get(vacina);
			if (qtd == null) qtd = 0;
			qtd = qtd + 1;
			lotesPorVacina.put(vacina, qtd);
		}		

		System.out.printf("UBSs: %d\t(%s)%n", qtdUnidades, (qtdUnidades >= MINIMO_UNIDADES ? "OK" : "NOT!!!"));
		System.out.printf("Servidores: %d\t(%s)%n", qtdServidores, (qtdServidores >= MINIMO_SERVIDORES ? "OK" : "NOT!!!"));
		System.out.printf("Vacinas: %d\t(%s)%n", qtdVacinas, (qtdVacinas >= MINIMO_VACINAS ? "OK" : "NOT!!!"));
		System.out.printf("Doenças: %d\t(%s)%n", qtdDoencas, (qtdDoencas >= MINIMO_DOENCAS ? "OK" : "NOT!!!"));
		System.out.printf("Agendamentos: %d\t(%s)%n", qtdAgendamentos, (qtdAgendamentos >= MINIMO_AGENDAMENTOS ? "OK" : "NOT!!!"));

		System.out.println("Lotes por vacina:");
		for (Map.Entry<Vacina, Integer> entry : lotesPorVacina.entrySet())
			System.out.printf("\t%d\t(%s, %s)%n", entry.getValue(), (entry.getValue() >= MINIMO_LOTES_POR_VACINA ? "OK" : "NOT!!!"), entry.getKey().toString());

		System.out.println("\nDone!");
	}
}
