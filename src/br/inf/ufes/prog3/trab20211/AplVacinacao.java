package br.inf.ufes.prog3.trab20211;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import java.util.SortedSet;

import br.inf.ufes.prog3.trab20211.dominio.Cidadao;
import br.inf.ufes.prog3.trab20211.dominio.Doenca;
import br.inf.ufes.prog3.trab20211.dominio.UBS;
import br.inf.ufes.prog3.trab20211.exceptions.DomainException;
import br.inf.ufes.prog3.trab20211.io.Escritor;
import br.inf.ufes.prog3.trab20211.io.Leitor;

/**
 * Classe principal que executa o programa.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class AplVacinacao implements Serializable {
	/** Nome do arquivo no qual o estado da aplicação é serializado. */
	private static final String ARQUIVO_SERIALIZACAO = "dados.dat";

	/** Nome do arquivo de relatório de vacinação por UBS. */
	private static final String RELATORIO_VACINACAO = "1-vacinacao.csv";

	/** Nome do arquivo de relatório de entrega de vacinas por doença. */
	private static final String RELATORIO_ENTREGAS = "2-entregas.csv";

	/** Nome do arquivo de relatório de comunicados aos cidadãos vacinados. */
	private static final String RELATORIO_COMUNICADOS = "3-comunicados.csv";

	/** Listagem de UBSs para relatório de vacinação. */
	private SortedSet<UBS> unidades;

	/** Listagem de doenças para relatório de entregas. */
	private SortedSet<Doenca> doencas;

	/** Listagem de cidadãos para relatório de comunicados. */
	private SortedSet<Cidadao> cidadaos;

	/**
	 * Indica se o programa deve serializar os dados ao invés de gerar as saí­das.
	 */
	private boolean serializar;

	/** Método principal que executa o programa. */
	public static void main(String[] args) {
		AplVacinacao apl = null;
		boolean readOnly = false, writeOnly = false;
		String nomeArquivoUnidades = null, nomeArquivoServidores = null, nomeArquivoVacinas = null,
				nomeArquivoLotes = null, nomeArquivoVacinacao = null;

		// Determina configurações regionais.
		Locale.setDefault(new Locale("pt", "BR"));

		try {
			// Processa os parâmetros da linha de comando.
			for (int i = 0; i < args.length; i++) {
				// Procura pela opção -p, que especifica o arquivo de UBSs.
				if ("-u".equals(args[i]) && args.length > i + 1)
					nomeArquivoUnidades = args[i + 1];

				// Procura pela opção -d, que especifica o arquivo de servidores.
				else if ("-s".equals(args[i]) && args.length > i + 1)
					nomeArquivoServidores = args[i + 1];

				// Procura pela opção -o, que especifica o arquivo de vacinas.
				else if ("-v".equals(args[i]) && args.length > i + 1)
					nomeArquivoVacinas = args[i + 1];

				// Procura pela opção -e, que especifica o arquivo de lotes de vacinas.
				else if ("-l".equals(args[i]) && args.length > i + 1)
					nomeArquivoLotes = args[i + 1];

				// Procura pela opção -m, que especifica o arquivo de agendamentos de vacinação.
				else if ("-a".equals(args[i]) && args.length > i + 1)
					nomeArquivoVacinacao = args[i + 1];

				// Procura pela opções --read-only e --write-only, que indicam o uso de
				// serialização.
				else if ("--read-only".equals(args[i]))
					readOnly = true;
				else if ("--write-only".equals(args[i]))
					writeOnly = true;
			}

			// Cria o escritor.
			Escritor escritor = new Escritor(new File(ARQUIVO_SERIALIZACAO), new File(RELATORIO_VACINACAO), new File(RELATORIO_ENTREGAS), new File(RELATORIO_COMUNICADOS));

			// Se os nomes dos arquivos não foram especificados, imprime mensagem de erro.
			if (!writeOnly && (nomeArquivoUnidades == null || nomeArquivoServidores == null
					|| nomeArquivoVacinas == null || nomeArquivoLotes == null || nomeArquivoVacinacao == null))
				System.out.printf(
						"Arquivos de entrada não especificados. Use: -u <arquivo> -s <arquivo> -v <arquivo> -l <arquivo> -a <arquivo>%n");

			// Do contrário, executa a aplicação. Verifica primeiro se devemos restaurá-la
			// por serialização.
			else if (writeOnly) {
				apl = escritor.desserializar();
				apl.serializar = false;
			}

			// Se não é pra restaurar a aplicação serializada, cria uma nova aplicação e lê
			// os dados dos arquivos.
			else {
				Leitor leitor = new Leitor(new File(nomeArquivoUnidades), new File(nomeArquivoServidores),
						new File(nomeArquivoVacinas), new File(nomeArquivoLotes), new File(nomeArquivoVacinacao));
				apl = new AplVacinacao(readOnly, leitor.listarUnidades(), leitor.listarDoencas(), leitor.listarCidadaos());
			}

			// Finalmente, executa a aplicação.
			if (apl != null)
				apl.executar(escritor);
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("Erro de I/O.");
			//e.printStackTrace();
		} catch (DomainException e) {
			System.out.printf("%s%n", e.getMessage());
			//e.printStackTrace();
		}

		//System.out.println("End");
	}

	/** Construtor. */
	public AplVacinacao(boolean readOnly, SortedSet<UBS> unidades, SortedSet<Doenca> doencas, SortedSet<Cidadao> cidadaos) {
		this.serializar = readOnly;
		this.unidades = unidades;
		this.doencas = doencas;
		this.cidadaos = cidadaos;
	}

	/**
	 * Produz a saí­da, seja serializando a aplicação (read-only) ou escrevendo os
	 * relatórios.
	 * 
	 * @param escritor Objeto responsável pela escrita dos relatórios.
	 * @throws IOException No caso de erros de escrita (acesso aos arquivos de
	 *                     relatório).
	 */
	private void executar(Escritor escritor) throws IOException {
		// Verifica se é pra serializar ou pra produzir os relatórios.
		if (serializar)
			escritor.serializar(this);
		else {
			// Escreve os relatórios.
			escritor.escreverRelatorioVacinacao(unidades);
			escritor.escreverRelatorioEntregas(doencas);
			escritor.escreverRelatorioComunicados(cidadaos);
		}
	}
}
