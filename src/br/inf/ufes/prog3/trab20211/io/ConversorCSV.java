package br.inf.ufes.prog3.trab20211.io;

import java.util.List;

import br.inf.ufes.prog3.trab20211.exceptions.DomainException;

public interface ConversorCSV<T> {
	void criarObjetoDeLinhaCSV(String[] dados, List<T> lista) throws DomainException;
}
