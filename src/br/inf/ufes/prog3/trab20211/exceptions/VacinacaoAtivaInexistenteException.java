package br.inf.ufes.prog3.trab20211.exceptions;

import br.inf.ufes.prog3.trab20211.dominio.Cidadao;

public class VacinacaoAtivaInexistenteException extends DomainException {
	private Cidadao cidadao;

	public VacinacaoAtivaInexistenteException(Cidadao cidadao) {
		this.cidadao = cidadao;
	}

	@Override
	public String getMessage() {
		return "Cidadão não possui agendamento ativo: " + cidadao + ".";
	}
}
