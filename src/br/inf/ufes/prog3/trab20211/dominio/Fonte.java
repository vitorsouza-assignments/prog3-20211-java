package br.inf.ufes.prog3.trab20211.dominio;

public enum Fonte {
	ESTADUAL,

	FEDERAL;

	public static Fonte daSigla(String sigla) {
		switch (sigla) {
			case "E":
				return ESTADUAL;
			case "F":
				return FEDERAL;
		}

		// Em caso de sigla desconhecida.
		throw new IllegalArgumentException("Sigla de fonte inválida: " + sigla);
	}
}
