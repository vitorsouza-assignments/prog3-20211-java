package br.inf.ufes.prog3.trab20211.exceptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.inf.ufes.prog3.trab20211.dominio.UBS;
import br.inf.ufes.prog3.trab20211.dominio.Vacina;

public class UBSSemEstoqueException extends DomainException {
	private static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

	private UBS unidade;

	private Vacina vacina;

	private Date data;

	public UBSSemEstoqueException(UBS unidade, Vacina vacina, Date data) {
		this.unidade = unidade;
		this.vacina = vacina;
		this.data = data;
	}

	@Override
	public String getMessage() {
		return "" + unidade + " não possui a vacina " + vacina + " em estoque em " + FORMATA_DATAS.format(data) + ".";
	}
}
