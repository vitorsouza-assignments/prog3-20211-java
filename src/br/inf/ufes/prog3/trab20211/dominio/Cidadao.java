package br.inf.ufes.prog3.trab20211.dominio;

import java.text.ParseException;
import java.util.Date;

import br.inf.ufes.prog3.trab20211.exceptions.ServidorNaoPertenceAUBSException;
import br.inf.ufes.prog3.trab20211.exceptions.UBSSemEstoqueException;
import br.inf.ufes.prog3.trab20211.exceptions.VacinacaoAtivaInexistenteException;

public class Cidadao extends Pessoa {
	private long cpf;

	private Vacinacao agendamentoAtivo;

	private Vacina vacina;

	private Date dataVacinacao;

	private boolean duasDoses;

	public Cidadao(String nome, Date dataNascimento, long cpf) {
		super(nome, dataNascimento);
		this.cpf = cpf;
	}

	public Vacina getVacina() {
		return vacina;
	}

	public Date getDataVacinacao() {
		return dataVacinacao;
	}

	public boolean recebeuDuasDoses() {
		return duasDoses;
	}

	public String getCpfFormatado() {
		try {
			javax.swing.text.MaskFormatter formatter = new javax.swing.text.MaskFormatter("###.###.###-##");
			formatter.setValidCharacters("0123456789");
			formatter.setValueContainsLiteralCharacters(false);
			return formatter.valueToString(cpf);
		} catch (ParseException e) {
			return "" + cpf;
		}
	}

	public void agendarVacinacao(Vacinacao vacinacao) {
		this.agendamentoAtivo = vacinacao;
	}

	public void cancelarVacinacao() throws VacinacaoAtivaInexistenteException {
		if (agendamentoAtivo == null)
			throw new VacinacaoAtivaInexistenteException(this);
		agendamentoAtivo.cancelar();
		agendamentoAtivo = null;
	}

	public void registrarVacinacao(Servidor servidor)
			throws ServidorNaoPertenceAUBSException, UBSSemEstoqueException, VacinacaoAtivaInexistenteException {
		if (agendamentoAtivo == null)
			throw new VacinacaoAtivaInexistenteException(this);

		// Registra a vacinação. Se já tem registrada, registra segunda dose.
		if (vacina == null) {
			vacina = agendamentoAtivo.getVacina();
			dataVacinacao = agendamentoAtivo.getDataHora();
		} else
			duasDoses = true;

		// Registra o servidor que efetuou a vacinação e remove o agendamento ativo.
		agendamentoAtivo.registrar(servidor);
		agendamentoAtivo = null;
	}
}
