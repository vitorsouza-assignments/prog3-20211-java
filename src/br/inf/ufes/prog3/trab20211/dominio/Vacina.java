package br.inf.ufes.prog3.trab20211.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

public abstract class Vacina implements Comparable<Vacina>, Serializable {
	private String nome;

	private String fabricante;

	private Doenca doenca;

	private SortedSet<Lote> lotes = new TreeSet<>();

	protected Vacina(String nome, String fabricante, Doenca doenca) {
		this.nome = nome;
		this.fabricante = fabricante;
		this.doenca = doenca;
		doenca.registrarVacina(this);
	}

	public String getNome() {
		return nome;
	}

	protected String getFabricante() {
		return fabricante;
	}

	protected SortedSet<Lote> getLotes() {
		return Collections.unmodifiableSortedSet(lotes);
	}

	protected void registrarLote(Lote lote) {
		lotes.add(lote);
	}

	@Override
	public String toString() {
		return nome + " (" + doenca + ")";
	}

	public abstract String gerarComunicacao(Date dataVacinacao, boolean duasDoses);

	@Override
	public int compareTo(Vacina o) {
		// Compara por ordem alfabética de nome.
		Collator collator = Collator.getInstance(Locale.getDefault());
		collator.setStrength(Collator.PRIMARY);
		return collator.compare(nome, o.nome);
	}
}
