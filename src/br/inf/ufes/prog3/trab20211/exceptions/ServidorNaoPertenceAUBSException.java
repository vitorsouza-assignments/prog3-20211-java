package br.inf.ufes.prog3.trab20211.exceptions;

import br.inf.ufes.prog3.trab20211.dominio.Servidor;
import br.inf.ufes.prog3.trab20211.dominio.UBS;

public class ServidorNaoPertenceAUBSException extends DomainException {
	private UBS unidade;

	private Servidor servidor;

	public ServidorNaoPertenceAUBSException(UBS unidade, Servidor servidor) {
		this.unidade = unidade;
		this.servidor = servidor;
	}

	@Override
	public String getMessage() {
		return "Servidor " + servidor + " não está lotado na " + unidade + ".";
	}
}
