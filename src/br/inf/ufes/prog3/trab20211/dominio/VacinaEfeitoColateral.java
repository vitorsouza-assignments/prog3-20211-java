package br.inf.ufes.prog3.trab20211.dominio;

import java.util.Date;
import java.util.SortedSet;

public class VacinaEfeitoColateral extends Vacina {
	private static final String COMUNICACAO = "Atenção aos possíveis efeitos colaterais da vacina %s: %s. Se persistirem, procure a UBS.";

	private SortedSet<String> efeitosPossiveis;

	public VacinaEfeitoColateral(String nome, String fabricante, Doenca doenca, SortedSet<String> efeitosPossiveis) {
		super(nome, fabricante, doenca);
		this.efeitosPossiveis = efeitosPossiveis;
	}

	@Override
	public String gerarComunicacao(Date dataVacinacao, boolean duasDoses) {
		StringBuilder builder = new StringBuilder();
		for (String efeito : efeitosPossiveis) {
			builder.append(efeito);
			builder.append(", ");
		}
		if (builder.length() > 1)
			builder.delete(builder.length() - 2, builder.length());
		return String.format(COMUNICACAO, getNome(), builder.toString());
	}
}
