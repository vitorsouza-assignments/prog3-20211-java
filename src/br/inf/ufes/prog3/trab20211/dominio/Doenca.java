package br.inf.ufes.prog3.trab20211.dominio;

import java.io.Serializable;
import java.text.Collator;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class Doenca implements Comparable<Doenca>, Serializable {
	private String nome;

	private SortedSet<Vacina> vacinas = new TreeSet<>();

	public Doenca(String nome) {
		this.nome = nome;
	}

	protected void registrarVacina(Vacina vacina) {
		vacinas.add(vacina);
	}

	public String getListaVacinas() {
		// Adiciona as vacinas e, se houver, fabricantes, separados por vírgula.
		StringBuilder builder = new StringBuilder();
		for (Vacina vacina : vacinas) {
			builder.append(vacina.getNome());
			if (vacina.getFabricante() != null)
				builder.append("/").append(vacina.getFabricante());
			builder.append(", ");
		}

		// Remove a última vírgula e retorna a string.
		if (builder.length() > 1)
			builder.delete(builder.length() - 2, builder.length());
		return builder.toString();
	}

	public long calcularQuantidadeDosesRecebidas() {
		long qtd = 0;
		for (Vacina vacina : vacinas) {
			for (Lote lote : vacina.getLotes()) {
				qtd += lote.getQuantidade();
			}
		}
		return qtd;
	}

	public double calcularCustoMedioDoses(Fonte fonte) {
		long qtdTotal = 0;
		double soma = 0;
		for (Vacina vacina : vacinas) {
			for (Lote lote : vacina.getLotes()) {
				if (lote.getFonte() == fonte) {
					qtdTotal += lote.getQuantidade();
					soma += lote.getCusto() * lote.getQuantidade();
				}
			}
		}
		return (qtdTotal == 0) ? 0 : soma / qtdTotal;
	}

	public double calcularMediaIntervaloEntregas() {
		// Coloca todos os lotes num único conjunto ordenado.
		SortedSet<Lote> todosOsLotes = new TreeSet<>();
		for (Vacina vacina : vacinas)
			todosOsLotes.addAll(vacina.getLotes());

		// Processa os lotes independente de vacina.
		Lote loteAnterior = null;
		double soma = 0;
		int qtdIntervalos = 0;
		for (Lote lote : todosOsLotes) {
			// Se há um lote anterior a este, calcula a diferença em dias e soma.
			if (loteAnterior != null) {
				long diffInMillies = Math.abs(lote.getData().getTime() - loteAnterior.getData().getTime());
				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
				soma += diff;
				qtdIntervalos++;
			}

			// Este lote passa a ser o lote anterior.
			loteAnterior = lote;
		}

		return (qtdIntervalos == 0) ? 0 : soma / qtdIntervalos;
	}

	@Override
	public String toString() {
		return nome;
	}

	@Override
	public int compareTo(Doenca o) {
		// Compara por ordem alfabética de nome.
		Collator collator = Collator.getInstance(Locale.getDefault());
		collator.setStrength(Collator.PRIMARY);
		return collator.compare(nome, o.nome);
	}
}
