package br.inf.ufes.prog3.trab20211.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.SortedSet;

import br.inf.ufes.prog3.trab20211.AplVacinacao;
import br.inf.ufes.prog3.trab20211.dominio.Cidadao;
import br.inf.ufes.prog3.trab20211.dominio.Doenca;
import br.inf.ufes.prog3.trab20211.dominio.Fonte;
import br.inf.ufes.prog3.trab20211.dominio.UBS;
import br.inf.ufes.prog3.trab20211.dominio.Vacina;

/**
 * Classe responsável pela escrita de relatórios em arquivos.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Escritor {
	/** Formatador de percentuais. */
	private static final NumberFormat FORMATA_PERCENTUAL = NumberFormat.getPercentInstance();
	static {
		FORMATA_PERCENTUAL.setMaximumFractionDigits(0);
	}

	/** Formatador de datas. */
	private static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

	/** Separador de colunas CSV. */
	private static final String SEPARADOR_CSV = ";";

	/** Cabeçalho do arquivo de relatório de vacinação por UBS. */
	private static final String CABECALHO_RELATORIO_VACINACAO = "UBS;Vacinados;A Vacinar;Agendamentos Cancelados;Período";

	/** Cabeçalho do arquivo de relatório de entregas de vacina por doença. */
	private static final String CABECALHO_RELATORIO_ENTREGAS = "Doença;Vacinas;Doses Recebidas;Custo Médio da Dose (Federal);Custo Médio da Dose (Estadual);Intervalo Médio Entregas (Dias)";

	/** Cabeçalho do arquivo de relatório de comunicados aos cidadãos vacinados. */
	private static final String CABECALHO_RELATORIO_COMUNICADOS = "Cidadão;CPF;Idade;Comunicado";

	/** Arquivo onde a aplicação deve ser (des)serializada. */
	private File arquivoSerializacao;

	/** Arquivo no qual será escrito o relatório de vacinação por UBS. */
	private File arquivoRelatorioVacinacao;

	/** Arquivo no qual será escrito o relatório de entrega de vacina por doença. */
	private File arquivoRelatorioEntregas;

	/** Arquivo no qual será escrito o relatório de comunicados aos cidadãos vacinados. */
	private File arquivoRelatorioComunicados;

	/** Construtor. */
	public Escritor(File arquivoSerializacao, File arquivoRelatorioVacinacao, File arquivoRelatorioEntregas, File arquivoRelatorioComunicados) {
		this.arquivoSerializacao = arquivoSerializacao;
		this.arquivoRelatorioVacinacao = arquivoRelatorioVacinacao;
		this.arquivoRelatorioEntregas = arquivoRelatorioEntregas;
		this.arquivoRelatorioComunicados = arquivoRelatorioComunicados;
	}

	/**
	 * Serializa a aplicação e todos os dados já calculados.
	 * 
	 * @param aplicacao Aplicação a ser serializada.
	 * @throws IOException No caso de erros de entrada e saída na escrita.
	 */
	public void serializar(AplVacinacao aplicacao) throws IOException {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(arquivoSerializacao))) {
			out.writeObject(aplicacao);
		}
	}

	/**
	 * Desserializa a aplicação e todos os dados já calculados.
	 * 
	 * @return A aplicação restaurada.
	 * @throws IOException            No caso de erros de entrada e saída na
	 *                                leitura.
	 * @throws ClassNotFoundException No caso de ler alguma classe no arquivo que
	 *                                não se encontra no classpath.
	 */
	public AplVacinacao desserializar() throws IOException, ClassNotFoundException {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(arquivoSerializacao))) {
			return (AplVacinacao) in.readObject();
		}
	}

	/**
	 * Escreve o relatório de vacinação por UBS.
	 * 
	 * @throws IOException No caso de qualquer problema na escrita do arquivo.
	 */
	public void escreverRelatorioVacinacao(SortedSet<UBS> unidades) throws IOException {
		try (PrintWriter out = new PrintWriter(arquivoRelatorioVacinacao)) {
			out.printf("%s%n", CABECALHO_RELATORIO_VACINACAO);

			// Passa pelas unidades, imprimindo as informações.
			for (UBS unidade : unidades) {
				String periodo = (unidade.getInicioVacinacao() == null || unidade.getFimVacinacao() == null) ? "-" : FORMATA_DATAS.format(unidade.getInicioVacinacao()) + " a " + FORMATA_DATAS.format(unidade.getFimVacinacao());
				out.printf("%s%s%d%s%d%s%d%s%s%n",
					unidade, SEPARADOR_CSV,
					unidade.getQtdCidadaosVacinados(), SEPARADOR_CSV,
					unidade.getQtdCidadaosAVacinar(), SEPARADOR_CSV,
					unidade.getQtdAgendamentosCancelados(), SEPARADOR_CSV,
					periodo);
			}
		}
	}

	/**
	 * Escreve o relatório de entrega de vacina por doença.
	 * 
	 * @throws IOException No caso de qualquer problema na escrita do arquivo.
	 */
	public void escreverRelatorioEntregas(SortedSet<Doenca> doencas) throws IOException {
		try (PrintWriter out = new PrintWriter(arquivoRelatorioEntregas)) {
			out.printf("%s%n", CABECALHO_RELATORIO_ENTREGAS);

			// Passa pelas doenças, imprimindo as informações.
			for (Doenca doenca : doencas) {
				out.printf("%s%s%s%s%d%s%.2f%s%.2f%s%.1f%n",
					doenca, SEPARADOR_CSV,
					doenca.getListaVacinas(), SEPARADOR_CSV,
					doenca.calcularQuantidadeDosesRecebidas(), SEPARADOR_CSV,
					doenca.calcularCustoMedioDoses(Fonte.FEDERAL), SEPARADOR_CSV,
					doenca.calcularCustoMedioDoses(Fonte.ESTADUAL), SEPARADOR_CSV,
					doenca.calcularMediaIntervaloEntregas());
			}
		}
	}

	/**
	 * Escreve o relatório de comunicação aos cidadãos vacinados.
	 * 
	 * @throws IOException No caso de qualquer problema na escrita do arquivo.
	 */
	public void escreverRelatorioComunicados(SortedSet<Cidadao> cidadaos) throws IOException {
		try (PrintWriter out = new PrintWriter(arquivoRelatorioComunicados)) {
			out.printf("%s%n", CABECALHO_RELATORIO_COMUNICADOS);

			// Passa pelos cidadãos vacinados, imprimindo as informações.
			for (Cidadao cidadao : cidadaos) {
				Vacina vacina = cidadao.getVacina();
				if (vacina != null) {
					out.printf("%s%s%s%s%d%s%s%n",
						cidadao, SEPARADOR_CSV,
						cidadao.getCpfFormatado(), SEPARADOR_CSV,
						cidadao.calcularIdade(), SEPARADOR_CSV,
						vacina.gerarComunicacao(cidadao.getDataVacinacao(), cidadao.recebeuDuasDoses()));
				}
			}
		}
	}
}
