package br.inf.ufes.prog3.trab20211.exceptions;

public class ReferenciaInvalidaException extends DomainException {
	private String referencia;

	public ReferenciaInvalidaException(String referencia) {
		this.referencia = referencia;
	}

	@Override
	public String getMessage() {
		return "Referência inválida: " + referencia + ".";
	}
}
