package br.inf.ufes.prog3.trab20211.dominio;

import java.io.Serializable;
import java.util.Date;

import br.inf.ufes.prog3.trab20211.exceptions.ServidorNaoPertenceAUBSException;
import br.inf.ufes.prog3.trab20211.exceptions.UBSSemEstoqueException;

public class Vacinacao implements Serializable {
	private Date dataHora;

	private UBS ubs;

	private Vacina vacina;

	private Cidadao cidadao;

	private Servidor servidor;

	private boolean efetuado;

	private boolean cancelado;

	public Vacinacao(Date dataHora, UBS ubs, Vacina vacina, Cidadao cidadao) {
		this.dataHora = dataHora;
		this.ubs = ubs;
		this.vacina = vacina;
		this.cidadao = cidadao;
		cidadao.agendarVacinacao(this);
		ubs.agendarVacinacao(this);
	}

	protected Date getDataHora() {
		return dataHora;
	}

	protected Vacina getVacina() {
		return vacina;
	}

	protected Cidadao getCidadao() {
		return cidadao;
	}

	protected boolean isEfetuado() {
		return efetuado;
	}

	protected boolean isCancelado() {
		return cancelado;
	}

	public void cancelar() {
		cancelado = true;
	}

	public void registrar(Servidor servidor) throws ServidorNaoPertenceAUBSException, UBSSemEstoqueException {
		// Verifica se o servidor trabalha na UBS.
		if (servidor.getUbs() != ubs)
			throw new ServidorNaoPertenceAUBSException(ubs, servidor);

		// Aplica a vacina.
		ubs.vacinar(this);

		this.servidor = servidor;
		efetuado = true;
	}

	@Override
	public String toString() {
		String status = (efetuado) ? "efetuado por " + servidor : (cancelado) ? "cancelado" : "ativo";
		return "Agendamento de " + cidadao + " na " + ubs + " em " + dataHora + " (status: " + status + ")";
	}
}
