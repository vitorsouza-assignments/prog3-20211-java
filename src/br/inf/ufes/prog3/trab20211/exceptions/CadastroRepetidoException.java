package br.inf.ufes.prog3.trab20211.exceptions;

public class CadastroRepetidoException extends DomainException {
	private String referencia;

	public CadastroRepetidoException(String referencia) {
		this.referencia = referencia;
	}

	@Override
	public String getMessage() {
		return "Cadastro repetido: " + referencia + ".";
	}
}
