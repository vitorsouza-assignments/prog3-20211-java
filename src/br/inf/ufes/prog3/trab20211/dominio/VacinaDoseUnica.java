package br.inf.ufes.prog3.trab20211.dominio;

import java.util.Date;

public class VacinaDoseUnica extends Vacina {
	private static final String COMUNICACAO = "Atenção, de acordo com o estudo disponível em %s, não é necessária uma segunda dose da vacina %s!";

	private String linkEstudo;

	public VacinaDoseUnica(String nome, String fabricante, Doenca doenca, String linkEstudo) {
		super(nome, fabricante, doenca);
		this.linkEstudo = linkEstudo;
	}

	@Override
	public String gerarComunicacao(Date dataVacinacao, boolean duasDoses) {
		return String.format(COMUNICACAO, linkEstudo, getNome());
	}
}
