package br.inf.ufes.prog3.trab20211.dominio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class VacinaDuasDoses extends Vacina {
	private static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

	private static final String MENSAGEM_PENDENTE = "Fique atento, você deve agendar para tomar a segunda dose da vacina %s entre os dias %s e %s.";

	private static final String MENSAGEM_COMPLETO = "Parabéns, você já tomou as duas doses da vacina %s!";

	private int diasSegundaDoseMin;

	private int diasSegundaDoseMax;

	public VacinaDuasDoses(String nome, String fabricante, Doenca doenca, int diasSegundaDoseMin,
			int diasSegundaDoseMax) {
		super(nome, fabricante, doenca);
		this.diasSegundaDoseMin = diasSegundaDoseMin;
		this.diasSegundaDoseMax = diasSegundaDoseMax;
	}

	@Override
	public String gerarComunicacao(Date dataVacinacao, boolean duasDoses) {
		if (duasDoses) {
			return String.format(MENSAGEM_COMPLETO, getNome());
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dataVacinacao);
			calendar.add(Calendar.DAY_OF_MONTH, diasSegundaDoseMin);
			Date dataSegundaDoseMin = calendar.getTime();
			calendar.add(Calendar.DAY_OF_MONTH, diasSegundaDoseMax - diasSegundaDoseMin);
			Date dataSegundaDoseMax = calendar.getTime();
			return String.format(MENSAGEM_PENDENTE, getNome(), FORMATA_DATAS.format(dataSegundaDoseMin),
					FORMATA_DATAS.format(dataSegundaDoseMax));
		}
	}
}
