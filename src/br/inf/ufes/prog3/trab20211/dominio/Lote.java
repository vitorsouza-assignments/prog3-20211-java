package br.inf.ufes.prog3.trab20211.dominio;

import java.io.Serializable;
import java.util.Date;

public class Lote implements Comparable<Lote>, Serializable {
	private Vacina vacina;

	private UBS ubs;

	private Date data;

	private long quantidade;

	private long quantidadeEmEstoque;

	private double custo;

	private Fonte fonte;

	public Lote(Vacina vacina, UBS ubs, Date data, long quantidade, double custo, Fonte fonte) {
		this.vacina = vacina;
		this.ubs = ubs;
		this.data = data;
		this.quantidade = this.quantidadeEmEstoque = quantidade;
		this.custo = custo;
		this.fonte = fonte;

		// Registra o lote junto à vacina.
		vacina.registrarLote(this);

		// Adiciona as vacinas ao estoque da UBS.
		ubs.receberLote(this);
	}

	public Vacina getVacina() {
		return vacina;
	}

	protected Date getData() {
		return data;
	}

	protected long getQuantidade() {
		return quantidade;
	}

	protected long getQuantidadeEmEstoque() {
		return quantidadeEmEstoque;
	}

	protected Fonte getFonte() {
		return fonte;
	}

	protected double getCusto() {
		return custo;
	}

	protected void retirarDose() {
		quantidadeEmEstoque--;
	}

	@Override
	public String toString() {
		return "" + quantidade + " de doses de " + vacina + " entregues para " + ubs + " em " + data;
	}

	@Override
	public int compareTo(Lote o) {
		// Ordena por data de entrega.
		int cmp = data.compareTo(o.data);
		if (cmp != 0)
			return cmp;

		// Em seguida, ordem de UBS.
		cmp = ubs.compareTo(o.ubs);
		if (cmp != 0)
			return cmp;

		// Em seguida, ordem de vacina.
		return vacina.compareTo(o.vacina);
	}
}
