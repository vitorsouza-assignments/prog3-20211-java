package br.inf.ufes.prog3.trab20211.exceptions;

public class DadoInvalidoException extends DomainException {
	private String dado;

	public DadoInvalidoException(String dado) {
		this.dado = dado;
	}

	@Override
	public String getMessage() {
		return "Dado inválido: " + dado + ".";
	}
}
