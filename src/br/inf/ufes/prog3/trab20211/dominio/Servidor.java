package br.inf.ufes.prog3.trab20211.dominio;

import java.util.Date;

public class Servidor extends Pessoa {
	private long matricula;

	private UBS ubs;

	public Servidor(String nome, long matricula, UBS ubs) {
		super(nome);
		this.matricula = matricula;
		this.ubs = ubs;
	}

	public Servidor(String nome, Date dataNascimento, long matricula, UBS ubs) {
		super(nome, dataNascimento);
		this.matricula = matricula;
		this.ubs = ubs;
	}

	public long getMatricula() {
		return matricula;
	}

	protected UBS getUbs() {
		return ubs;
	}

	@Override
	public String toString() {
		return super.toString() + " (Matrícula: " + matricula + ")";
	}
}
