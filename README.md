# README #

Trabalho Java da [disciplina de Programação III lecionada no semestre 2021/1](https://ava.ufes.br/course/view.php?id=16282) na [Universidade Federal do Espírito Santo](https://www.ufes.br/) pelo [prof. Vítor E. Silva Souza](http://www.inf.ufes.br/~vitorsouza/).

### Como compilar e executar ###

Utilize os alvos do [Apache Ant](http://ant.apache.org) para compilar e executar o programa:

* `ant compile`: compila o código, gerando a pasta `bin/`;
* `ant run`: executa o programa no modo padrão (lê os arquivos de entrada e produz os relatórios de saída);
* `ant run-read-only`: executa o programa em modo somente leitura (lê os arquivos de entrada e serializa os dados em `dados.dat`);
* `ant run-write-only`: executa o programa em modo somente escrita (lê o arquivo serializado `dados.dat` e produz os relatórios de saída);
* `ant clean`: exclui todos os arquivos gerados, mantendo apenas o código-fonte.