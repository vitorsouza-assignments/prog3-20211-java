package br.inf.ufes.prog3.trab20211.io;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.json.JSONException;
import org.json.JSONObject;

import br.inf.ufes.prog3.trab20211.dominio.Cidadao;
import br.inf.ufes.prog3.trab20211.dominio.Doenca;
import br.inf.ufes.prog3.trab20211.dominio.Fonte;
import br.inf.ufes.prog3.trab20211.dominio.Lote;
import br.inf.ufes.prog3.trab20211.dominio.Servidor;
import br.inf.ufes.prog3.trab20211.dominio.UBS;
import br.inf.ufes.prog3.trab20211.dominio.Vacina;
import br.inf.ufes.prog3.trab20211.dominio.VacinaDoseUnica;
import br.inf.ufes.prog3.trab20211.dominio.VacinaDuasDoses;
import br.inf.ufes.prog3.trab20211.dominio.VacinaEfeitoColateral;
import br.inf.ufes.prog3.trab20211.dominio.Vacinacao;
import br.inf.ufes.prog3.trab20211.exceptions.CadastroRepetidoException;
import br.inf.ufes.prog3.trab20211.exceptions.DadoInvalidoException;
import br.inf.ufes.prog3.trab20211.exceptions.DomainException;
import br.inf.ufes.prog3.trab20211.exceptions.ReferenciaInvalidaException;

/**
 * Classe responsável pela leitura dos dados das planilhas.
 *
 * @author Vítor E. Silva Souza (https://github.com/vitorsouza/)
 */
public class Leitor {
	/** Formatador de datas. */
	private static final DateFormat FORMATA_DATAS = new SimpleDateFormat("dd/MM/yyyy");

	/** Formatador de datas e horas. */
	private static final DateFormat FORMATA_DATAS_HORAS = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	/** Formatador de números. */
	private static final NumberFormat FORMATA_NUMEROS = NumberFormat.getNumberInstance();

	/** UBSs cadastradas, indexadas por sigla. */
	private Map<String, UBS> unidades = new HashMap<>();

	/** Servidores cadastrados, indexados por matrícula. */
	private Map<Long, Servidor> servidores = new HashMap<>();

	/** Doenças cadastradas, indexadas por nome. */
	private Map<String, Doenca> doencas = new HashMap<>();

	/** Vacinas cadastradas, indexadas por nome. */
	private Map<String, Vacina> vacinas = new HashMap<>();

	/** Lotes de vacina entregues às UBSs. */
	private Set<Lote> lotes = new HashSet<>();

	/** Cadastro de cidadãos, indexados por CPF. */
	private Map<Long, Cidadao> cidadaos = new HashMap<>();

	/** Vacinações agendadas, canceladas e efetuadas. */
	private Set<Vacinacao> vacinacoes = new HashSet<>();

	/** Construtor. */
	public Leitor(File nomeArquivoUnidades, File nomeArquivoServidores, File nomeArquivoVacinas, File nomeArquivoLotes,
			File nomeArquivoVacinacao) throws IOException, DomainException {
		// Lê os arquivos de dados na ordem correta.
		lerUnidades(nomeArquivoUnidades);
		lerServidores(nomeArquivoServidores);
		lerVacinas(nomeArquivoVacinas);
		lerLotes(nomeArquivoLotes);
		lerVacinacoes(nomeArquivoVacinacao);
	}

	public SortedSet<UBS> listarUnidades() {
		SortedSet<UBS> listagem = new TreeSet<>();
		listagem.addAll(unidades.values());
		return listagem;
	}

	public SortedSet<Doenca> listarDoencas() {
		SortedSet<Doenca> listagem = new TreeSet<>();
		listagem.addAll(doencas.values());
		return listagem;
	}

	public SortedSet<Cidadao> listarCidadaos() {
		SortedSet<Cidadao> listagem = new TreeSet<>();
		listagem.addAll(cidadaos.values());
		return listagem;
	}

	public int getQtdServidores() {
		return servidores.size();
	}

	public int getQtdVacinas() {
		return vacinas.size();
	}

	public Set<Lote> getLotes() {
		return Collections.unmodifiableSet(lotes);
	}

	public int getQtdVacinacoes() {
		return vacinacoes.size();
	}

	/* ================== MÉTODOS DE LEITURA. ================== */

	/**
	 * Lê o cadastro de UBSs.
	 * 
	 * @param arquivo Arquivo que contém os dados do cadastro de UBSs.
	 * @throws IOException    No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerUnidades(File arquivo) throws IOException, DomainException {
		// Obtém a lista de UBSs do arquivo.
		List<UBS> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_UNIDADES, lista);

		// Monta o mapa de UBSs por sigla.
		for (UBS objeto : lista) {
			String sigla = objeto.getSigla();

			// Verifica cadastro repetido antes de cadastrar.
			if (unidades.containsKey(sigla))
				throw new CadastroRepetidoException(sigla);
			unidades.put(sigla, objeto);
		}
	}

	/**
	 * Lê o cadastro de servidores.
	 * 
	 * @param arquivo Arquivo que contém os dados do cadastro de servidores.
	 * @throws IOException    No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerServidores(File arquivo) throws IOException, DomainException {
		// Obtém a lista de servidores do arquivo.
		List<Servidor> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_SERVIDORES, lista);

		// Monta o mapa de servidores por matrícula.
		for (Servidor objeto : lista) {
			Long matricula = objeto.getMatricula();

			// Verifica cadastro repetido antes de cadastrar.
			if (servidores.containsKey(matricula))
				throw new CadastroRepetidoException("" + matricula);
			servidores.put(matricula, objeto);
		}
	}

	/**
	 * Lê o cadastro de vacinas.
	 * 
	 * @param arquivo Arquivo que contém os dados do cadastro de vacinas.
	 * @throws IOException    No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerVacinas(File arquivo) throws IOException, DomainException {
		// Obtém a lista de servidores do arquivo.
		List<Vacina> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_VACINAS, lista);

		// Monta o mapa de servidores por matrícula.
		for (Vacina objeto : lista) {
			String nome = objeto.getNome();

			// Verifica cadastro repetido antes de cadastrar.
			if (vacinas.containsKey(nome))
				throw new CadastroRepetidoException(nome);
			vacinas.put(nome, objeto);
		}
	}

	/**
	 * Lê o cadastro de lotes.
	 * 
	 * @param arquivo Arquivo que contém os dados do cadastro de lotes.
	 * @throws IOException    No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerLotes(File arquivo) throws IOException, DomainException {
		// Obtém a lista de lotes do arquivo.
		List<Lote> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_LOTES, lista);

		// Adiciona todos no conjunto de lotes.
		lotes.addAll(lista);
	}

	/**
	 * Lê o cadastro de vacinações.
	 * 
	 * @param arquivo Arquivo que contém os dados do cadastro de vacinações.
	 * @throws IOException    No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException No caso de erros de formatação dos dados do arquivo.
	 */
	private void lerVacinacoes(File arquivo) throws IOException, DomainException {
		// Obtém a lista de lotes do arquivo.
		List<Vacinacao> lista = new ArrayList<>();
		lerArquivo(arquivo, CONVERSOR_CSV_VACINACAO, lista);

		// Adiciona todos no conjunto de lotes.
		vacinacoes.addAll(lista);
	}

	/**
	 * Método genérico para leitura de arquivos CSV. Os dados lidos do arquivo são
	 * convertidos para o objeto específico por meio de um conversor e colocados em
	 * uma lista.
	 * 
	 * @param arquivo   Arquivo que contém os dados em formato CSV.
	 * @param conversor Conversor responsável por criar um objeto a partir de uma
	 *                  linha CSV.
	 * @param lista     Lista onde os objetos criados serão colocados.
	 * @throws IOException    No caso de erros de leitura (acesso ao arquivo).
	 * @throws ParseException No caso de erros de formatação dos dados do arquivo.
	 */
	private <T> void lerArquivo(File arquivo, ConversorCSV<T> conversor, List<T> lista)
			throws IOException, DomainException {
		// Cria um scanner para ler o arquivo linha por linha.
		//System.out.println("\n\n\n" + arquivo.getName()); int l = 1;
		try (Scanner scanner = new Scanner(arquivo)) {
			// Despreza a primeira linha (título) e lê as demais.
			if (scanner.hasNextLine())
				scanner.nextLine();
			while (scanner.hasNextLine()) {
				//System.out.println(++l);
				String linha = scanner.nextLine();
				if ((linha != null) && (!linha.isEmpty())) {
					// Separa os dados conditos na linha pelos ponto-e-vírgula usados como
					// separadores.
					String[] dados = linha.split(";");

					// Remove espaços que estejam sobrando nas strings.
					for (int i = 0; i < dados.length; i++)
						if (dados[i] != null)
							dados[i] = dados[i].trim();

					// Cria um novo objeto a partir do conversor e adiciona-o à lista.
					conversor.criarObjetoDeLinhaCSV(dados, lista);
				}
			}
		}
	}

	/* ================== CONVERSORES CSV. ================== */

	/** Conversor CSV para UBSs. */
	private final ConversorCSV<UBS> CONVERSOR_CSV_UNIDADES = new ConversorCSV<>() {
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<UBS> lista) throws DomainException {
			// Cria a UBS e adiciona à lista.
			UBS ubs = new UBS(dados[0], dados[1]);
			lista.add(ubs);
		}
	};

	/** Conversor CSV para Servidores. */
	private final ConversorCSV<Servidor> CONVERSOR_CSV_SERVIDORES = new ConversorCSV<>() {
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Servidor> lista) throws DomainException {
			// Obtém a data de nascimento, se tiver sido fornecida.
			Date dataNascimento = null;
			if (dados[2] != null && !dados[2].isEmpty()) {
				try {
					dataNascimento = FORMATA_DATAS.parse(dados[2]);
				} catch (ParseException e) {
					throw new DadoInvalidoException(dados[2]);
				}
			}

			// Recupera a UBS do cadastro a partir da sigla.
			UBS ubs = unidades.get(dados[3]);
			if (ubs == null)
				throw new ReferenciaInvalidaException(dados[3]);

			// Cria o servidor e adiciona à lista.
			try {
				Servidor servidor = (dataNascimento == null) ? new Servidor(dados[0], Long.parseLong(dados[1]), ubs)
						: new Servidor(dados[0], dataNascimento, Long.parseLong(dados[1]), ubs);
				lista.add(servidor);
			} catch (NumberFormatException e) {
				throw new DadoInvalidoException(dados[1]);
			}
		}
	};

	/** Conversor CSV para vacinas. */
	private final ConversorCSV<Vacina> CONVERSOR_CSV_VACINAS = new ConversorCSV<>() {
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Vacina> lista) throws DomainException {
			// Verifica se a doença já está cadastrada, se não, cadastra.
			Doenca doenca = doencas.get(dados[2]);
			if (doenca == null) {
				doenca = new Doenca(dados[2]);
				doencas.put(dados[2], doenca);
			}

			// Verifica o tipo de vacina e cria a vacina adequada.
			Vacina vacina = null;
			switch (dados[3].trim()) {
				case "U":
					vacina = new VacinaDoseUnica(dados[0], dados[1], doenca, dados[4]);
					break;

				case "D":
					String[] diasSegundaDose = dados[4].split("-");
					try {
						vacina = new VacinaDuasDoses(dados[0], dados[1], doenca,
								Integer.parseInt(diasSegundaDose[0].trim()),
								Integer.parseInt(diasSegundaDose[1].trim()));
					} catch (NumberFormatException e) {
						throw new DadoInvalidoException(dados[4]);
					}
					break;

				case "E":
					SortedSet<String> efeitosPossiveis = new TreeSet<>();
					try {
						JSONObject jsonObj = new JSONObject(dados[4].trim());
						for (String chave : jsonObj.keySet()) {
							String efeito = chave + " (" + jsonObj.getString(chave) + ")";
							efeitosPossiveis.add(efeito);
						}
					} catch (JSONException e) {
						throw new DadoInvalidoException(dados[4]);
					}
					vacina = new VacinaEfeitoColateral(dados[0], dados[1], doenca, efeitosPossiveis);
					break;

				default:
					throw new DadoInvalidoException(dados[3]);
			}

			// Adiciona a vacina criada à lista.
			lista.add(vacina);
		}
	};

	/** Conversor CSV para entregas de lotes. */
	private final ConversorCSV<Lote> CONVERSOR_CSV_LOTES = new ConversorCSV<>() {
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Lote> lista) throws DomainException {
			// Recupera a vacina do cadastro a partir do nome.
			Vacina vacina = vacinas.get(dados[0]);
			if (vacina == null)
				throw new ReferenciaInvalidaException(dados[0]);

			// Recupera a UBS do cadastro a partir da sigla.
			UBS ubs = unidades.get(dados[1]);
			if (ubs == null)
				throw new ReferenciaInvalidaException(dados[1]);

			// Obtém a data de entrega do lote.
			Date data = null;
			try {
				data = FORMATA_DATAS.parse(dados[2]);
			} catch (ParseException e) {
				throw new DadoInvalidoException(dados[2]);
			}

			// Processa a quantidade recebida.
			long quantidade = 0;
			try {
				quantidade = Long.parseLong(dados[3]);
			} catch (NumberFormatException e) {
				throw new DadoInvalidoException(dados[3]);
			}

			// Processa o custo por dose.
			double custo = 0;
			try {
				custo = FORMATA_NUMEROS.parse(dados[4]).doubleValue();
			} catch (ParseException e) {
				throw new DadoInvalidoException(dados[4]);
			}

			// Recupera a fonte.
			Fonte fonte = null;
			try {
				fonte = Fonte.daSigla(dados[5].trim());
			} catch (IllegalArgumentException e) {
				throw new DadoInvalidoException(dados[5]);
			}

			// Cria o lote e adiciona à lista.
			Lote lote = new Lote(vacina, ubs, data, quantidade, custo, fonte);
			lista.add(lote);
		}
	};

	/** Conversor CSV para agendamentos de vacinação. */
	private final ConversorCSV<Vacinacao> CONVERSOR_CSV_VACINACAO = new ConversorCSV<>() {
		@Override
		public void criarObjetoDeLinhaCSV(String[] dados, List<Vacinacao> lista) throws DomainException {
			// Lê o CPF.
			long cpf = 0;
			try {
				cpf = Long.parseLong(dados[1]);
			} catch (NumberFormatException e) {
				throw new DadoInvalidoException(dados[1]);
			}

			// Verifica a operação, lê os dados adequados e efetua a operação.
			Cidadao cidadao = null;
			switch (dados[0].trim()) {
				/* AGENDAMENTO. */
				case "A":
					// Obtém a data/hora do agendamento.
					Date dataAgendamento = null;
					try {
						dataAgendamento = FORMATA_DATAS_HORAS.parse(dados[2]);
					} catch (ParseException e) {
						throw new DadoInvalidoException(dados[2]);
					}

					// Obtém a data de nascimento.
					Date dataNascimento = null;
					try {
						dataNascimento = FORMATA_DATAS.parse(dados[6]);
					} catch (ParseException e) {
						throw new DadoInvalidoException(dados[6]);
					}

					// Recupera a UBS do cadastro a partir da sigla.
					UBS ubs = unidades.get(dados[3]);
					if (ubs == null)
						throw new ReferenciaInvalidaException(dados[3]);

					// Recupera a vacina do cadastro a partir do nome.
					Vacina vacina = vacinas.get(dados[4]);
					if (vacina == null)
						throw new ReferenciaInvalidaException(dados[4]);

					// Verifica se o cidadão já está cadastrado, se não, cadastra.
					cidadao = cidadaos.get(cpf);
					if (cidadao == null) {
						cidadao = new Cidadao(dados[5], dataNascimento, cpf);
						cidadaos.put(cpf, cidadao);
					}

					// Cria a vacinação e adiciona à lista.
					Vacinacao vacinacao = new Vacinacao(dataAgendamento, ubs, vacina, cidadao);
					lista.add(vacinacao);
					break;

				/* CANCELAMENTO. */
				case "C":
					// Recupera o cidadão do cadastro a partir do CPF e cancela a vacinação.
					cidadao = cidadaos.get(cpf);
					if (cidadao == null)
						throw new ReferenciaInvalidaException(dados[1]);
					cidadao.cancelarVacinacao();
					break;

				/* REGISTRO. */
				case "R":
					// Recupera o servidor do cadastro a partir da matrícula.
					Servidor servidor = servidores.get(Long.parseLong(dados[7]));
					if (servidor == null)
						throw new ReferenciaInvalidaException(dados[7]);

					// Recupera o cidadão do cadastro a partir do CPF e registra a vacinação.
					cidadao = cidadaos.get(cpf);
					if (cidadao == null)
						throw new ReferenciaInvalidaException(dados[1]);
					cidadao.registrarVacinacao(servidor);
					break;

				default:
					throw new DadoInvalidoException(dados[0]);
			}
		}
	};
}
